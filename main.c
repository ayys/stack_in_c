/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  test stack implementation for errors
 *
 *        Version:  1.0
 *        Created:  2017 फेब्रुअरी 21 मङ्गलबार  19:58:49
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ayush Jha (aj), ayushjha6@gmail.com
 *        Company:  
 *
 * =====================================================================================
 */


#include "stack.h"

int main(){
  Stack stack = new_stack();
  StackElement *elem;
  int i;

  /* insert 100,000 elements */
  for (i = 0; i < 100000; i++){
    stack.push(&i, &stack);
  }

  /* traverse 100,000 elements */
  for (i = 0; i < 100000; i++){
    elem = stack.traverse(i, &stack);
    /* convert void pointer to int pointer and dereference it */
    printf("%d ", *((int *)elem->data));
  }
  return 0;
}
